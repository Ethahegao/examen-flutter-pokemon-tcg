import 'package:flutter/material.dart';
import 'package:tp_3/pages/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      title: 'FlutterCourseDemo',
      home: HomePage(),
    );
  }
}
