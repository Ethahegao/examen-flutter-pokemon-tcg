
class Attack {
  final List<String> cost;
  final String name;
  final String text;
  final String damage;
  final int convertedEnergyCost;

  Attack({this.cost, this.name, this.text, this.damage, this.convertedEnergyCost});
}
