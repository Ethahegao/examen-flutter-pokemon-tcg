class Weakness {
  final String type;
  final String value;

  Weakness({this.type, this.value});
}
