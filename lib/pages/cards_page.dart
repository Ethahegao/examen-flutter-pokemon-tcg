import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:tp_3/models/card_model.dart';
import 'package:tp_3/page_route.dart';
import 'package:tp_3/pages/card_detail_page.dart';
import 'package:tp_3/widgets/scaffold_widget.dart';

class CardsPage extends StatefulWidget {
  @override
  _CardsPageState createState() => _CardsPageState();
}

class _CardsPageState extends State<CardsPage> {
  bool _loading;
  Timer _debounce;
  String _currentName = '';
  int _currentPage = 1;
  List<PokeTcgCard> _pokeTcgCards = List<PokeTcgCard>();
  ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    _scrollController.addListener(() {
      if (_debounce?.isActive ?? false) _debounce.cancel();
      _debounce = Timer(Duration(milliseconds: 500), () {
        if (_scrollController.position.extentAfter == 0.0) _searchMorePokemon();
      });
    });

    return customScaffoldWidget(
      context: context,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_upward),
        onPressed: () => _scrollController.jumpTo(0.0),
      ),
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(32.0),
              child: TextField(
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.search), hintText: 'Pokemon name'),
                onChanged: (value) {
                  _currentName = value;
                  _searchPokemon();
                },
              ),
            ),
            _buildCardsList(),
          ],
        ),
      ),
    );
  }

  void _searchPokemon() async {
    if (_currentName == null || _currentName.length < 2) return;
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(Duration(milliseconds: 500), () async {
      setState(() => _loading = true);

      final response = await http.get(_getPokeTcgApiCardsUrl());

      if (response.statusCode == 200) {
        PokeTcgCard.fromJson(json.decode(response.body));
        final cards = jsonDecode(response.body)['cards'];

        _currentPage = 1;
        _pokeTcgCards = new List<PokeTcgCard>();

        for (Map card in cards) {
          _pokeTcgCards.add(PokeTcgCard.fromJson(card));
        }
        setState(() => _loading = false);
      }
    });
  }

  void _searchMorePokemon() async {
    setState(() => _loading = true);

    _currentPage++;

    final response = await http.get(_getPokeTcgApiCardsUrl());

    if (response.statusCode == 200) {
      PokeTcgCard.fromJson(json.decode(response.body));
      final cards = jsonDecode(response.body)['cards'];

      for (Map card in cards) {
        _pokeTcgCards.add(PokeTcgCard.fromJson(card));
      }
      setState(() => _loading = false);
    }
  }

  Widget _buildCardsList() {
    if (_pokeTcgCards.length < 1) return Container();

    return ListView.builder(
      shrinkWrap: true,
      physics: ScrollPhysics(),
      itemExtent: 64.0,
      itemCount: _pokeTcgCards.length,
      itemBuilder: (context, i) {
        return ListTile(
          title: Text(_pokeTcgCards[i].name),
          leading: CachedNetworkImage(
            imageUrl: _pokeTcgCards[i].imageUrl,
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) =>
                Icon(Icons.error, color: Colors.red),
          ),
          onTap: () => Navigator.push(
            context,
            CustomPageRoute(builder: (context) => CardDetailPage(pokeTcgCard: _pokeTcgCards[i])),
          ),
        );
      },
    );
  }

  String _getPokeTcgApiCardsUrl() {
    String pokeTcgApiCardsUrl =
        'https://api.pokemontcg.io/v1/cards?page=$_currentPage&pageSize=20';
    if (_currentName.isNotEmpty) pokeTcgApiCardsUrl += '&name=$_currentName';
    print(pokeTcgApiCardsUrl);
    return pokeTcgApiCardsUrl;
  }
}
