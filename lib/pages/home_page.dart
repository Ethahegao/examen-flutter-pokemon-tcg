import 'package:flutter/material.dart';
import 'package:tp_3/widgets/scaffold_widget.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return customScaffoldWidget(
      context: context,
      body: Padding(
        padding: EdgeInsets.all(32.0),
        child: Text(
          'WIP Flutter app powered by Pokémon TCG Developers API',
          textAlign: TextAlign.justify,
        ),
      ),
    );
  }
}
