import 'package:flutter/material.dart';

import 'appbar_widget.dart';
import 'drawer_widget.dart';

Widget customScaffoldWidget(
    {@required BuildContext context, @required Widget body, Widget floatingActionButton}) {
  return Scaffold(
    appBar: customAppBarWidget(),
    drawer: customDrawerWidget(context),
    floatingActionButton: floatingActionButton,
    body: body,
  );
}
